/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/buttonDebiuncer/buttonDebouncer/stanGry_test.vhd";



static void work_a_2083374895_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    int64 t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(69, ng0);

LAB3:    t1 = xsi_get_transient_memory(120U);
    memset(t1, 0, 120U);
    t2 = t1;
    memset(t2, (unsigned char)3, 120U);
    *((unsigned char *)t2) = (unsigned char)2;
    t2 = (t2 + 1U);
    *((unsigned char *)t2) = (unsigned char)3;
    t2 = (t2 + 1U);
    *((unsigned char *)t2) = (unsigned char)3;
    t2 = (t2 + 1U);
    t3 = (t0 + 2736);
    t4 = (t3 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    memcpy(t7, t1, 120U);
    xsi_driver_first_trans_delta(t3, 0U, 120U, 0LL);
    t8 = (200 * 1000LL);
    t9 = xsi_get_transient_memory(120U);
    memset(t9, 0, 120U);
    t10 = t9;
    memset(t10, (unsigned char)2, 120U);
    *((unsigned char *)t10) = (unsigned char)3;
    t10 = (t10 + 1U);
    *((unsigned char *)t10) = (unsigned char)3;
    t10 = (t10 + 1U);
    *((unsigned char *)t10) = (unsigned char)3;
    t10 = (t10 + 1U);
    t11 = (t0 + 2736);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    memcpy(t15, t9, 120U);
    xsi_driver_subsequent_trans_delta(t11, 0U, 120U, t8);

LAB2:
LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_2083374895_2372691052_init()
{
	static char *pe[] = {(void *)work_a_2083374895_2372691052_p_0};
	xsi_register_didat("work_a_2083374895_2372691052", "isim/stanGry_test_isim_beh.exe.sim/work/a_2083374895_2372691052.didat");
	xsi_register_executes(pe);
}
