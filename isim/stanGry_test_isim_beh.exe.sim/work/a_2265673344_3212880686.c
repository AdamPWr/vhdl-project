/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/buttonDebiuncer/buttonDebouncer/stanGry.vhd";



static void work_a_2265673344_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    int t6;
    int t7;
    int t8;
    int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned char t13;
    unsigned char t14;
    char *t15;
    char *t16;
    int t17;
    int t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;

LAB0:    xsi_set_current_line(46, ng0);
    t1 = (t0 + 3176);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t4 = (t3 + 56U);
    t5 = *((char **)t4);
    *((int *)t5) = 0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(47, ng0);
    t1 = (t0 + 5076);
    *((int *)t1) = 0;
    t2 = (t0 + 5080);
    *((unsigned int *)t2) = 120U;
    t6 = 0;
    t7 = 120U;

LAB2:    if (t6 <= t7)
        goto LAB3;

LAB5:    t1 = (t0 + 3080);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(48, ng0);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t3 = (t0 + 5076);
    t8 = *((int *)t3);
    t9 = (t8 - 119);
    t10 = (t9 * -1);
    xsi_vhdl_check_range_of_index(119, 0, -1, *((int *)t3));
    t11 = (1U * t10);
    t12 = (0 + t11);
    t5 = (t4 + t12);
    t13 = *((unsigned char *)t5);
    t14 = (t13 == (unsigned char)3);
    if (t14 != 0)
        goto LAB6;

LAB8:
LAB7:
LAB4:    t1 = (t0 + 5076);
    t6 = *((int *)t1);
    t2 = (t0 + 5080);
    t7 = *((unsigned int *)t2);
    if (t6 == t7)
        goto LAB5;

LAB9:    t8 = (t6 + 1);
    t6 = t8;
    t3 = (t0 + 5076);
    *((int *)t3) = t6;
    goto LAB2;

LAB6:    xsi_set_current_line(49, ng0);
    t15 = (t0 + 1352U);
    t16 = *((char **)t15);
    t17 = *((int *)t16);
    t18 = (t17 + 1);
    t15 = (t0 + 3176);
    t19 = (t15 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    *((int *)t22) = t18;
    xsi_driver_first_trans_fast(t15);
    goto LAB7;

}

static void work_a_2265673344_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    t1 = (t0 + 2760U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(55, ng0);
    t2 = (t0 + 1352U);
    t3 = *((char **)t2);
    t4 = *((int *)t3);
    if (t4 == 3)
        goto LAB5;

LAB7:
LAB6:    xsi_set_current_line(56, ng0);
    t2 = (t0 + 3240);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast_port(t2);

LAB4:    xsi_set_current_line(55, ng0);

LAB11:    t2 = (t0 + 3096);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB12;

LAB1:    return;
LAB5:    xsi_set_current_line(56, ng0);
    t2 = (t0 + 3240);
    t5 = (t2 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t2);
    goto LAB4;

LAB8:;
LAB9:    t3 = (t0 + 3096);
    *((int *)t3) = 0;
    goto LAB2;

LAB10:    goto LAB9;

LAB12:    goto LAB10;

}


extern void work_a_2265673344_3212880686_init()
{
	static char *pe[] = {(void *)work_a_2265673344_3212880686_p_0,(void *)work_a_2265673344_3212880686_p_1};
	xsi_register_didat("work_a_2265673344_3212880686", "isim/stanGry_test_isim_beh.exe.sim/work/a_2265673344_3212880686.didat");
	xsi_register_executes(pe);
}
