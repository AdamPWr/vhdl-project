----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:27:21 06/08/2020 
-- Design Name: 
-- Module Name:    stanGry - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity stanGry is
    Port ( SymbolsArray : in  STD_LOGIC_VECTOR (119 downto 0);
           IloscSymboliSieZgadza : out  STD_LOGIC);
end stanGry;

architecture Behavioral of stanGry is

signal licznik : integer := 0;

begin

liczenie_symboli : process(SymbolsArray)
   begin
		licznik <= 0;
		for i in 0 to SymbolsArray'length loop
			if SymbolsArray(i) = '1' then
				licznik <= licznik +1;
			end if;
		end loop;
	end process liczenie_symboli ;
	
	
	with licznik select IloscSymboliSieZgadza <=
	'1' when 3,
	'0' when others;
end Behavioral;

