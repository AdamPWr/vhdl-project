--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:51:54 06/07/2020
-- Design Name:   
-- Module Name:   D:/buttonDebiuncer/buttonDebouncer/debouncer_test.vhd
-- Project Name:  buttonDebouncer
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: debouncer
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY debouncer_test IS
END debouncer_test;
 
ARCHITECTURE behavior OF debouncer_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT debouncer
    PORT(
         Button : IN  std_logic;
         CLK : IN  std_logic;
         Is_Pressed : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal Button : std_logic := '0';
   signal CLK : std_logic := '0';

 	--Outputs
   signal Is_Pressed : std_logic;

   -- Clock period definitions
   constant CLK_period : time := 20 ns;
 
BEGIN
 

   uut: debouncer PORT MAP (
          Button => Button,
          CLK => CLK,
          Is_Pressed => Is_Pressed
        );
clk_process: process
   begin
        CLK <= '0';
        wait for CLK_period/2;  
        CLK <= '1';
        wait for CLK_period/2;
   end process;
	
	Button_process: process
   begin
        Button <= '1';
        wait for 10 ns;
        Button <= '0';
        wait for 300 ns;
   end process;

END;
