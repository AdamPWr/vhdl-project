--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:51:32 06/08/2020
-- Design Name:   
-- Module Name:   D:/buttonDebiuncer/buttonDebouncer/stanGry_test.vhd
-- Project Name:  buttonDebouncer
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: stanGry
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY stanGry_test IS
END stanGry_test;
 
ARCHITECTURE behavior OF stanGry_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT stanGry
    PORT(
         SymbolsArray : IN  std_logic_vector(119 downto 0);
         IloscSymboliSieZgadza : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal SymbolsArray : std_logic_vector(119 downto 0) := (others => '0');

 	--Outputs
   signal IloscSymboliSieZgadza : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: stanGry PORT MAP (
          SymbolsArray => SymbolsArray,
          IloscSymboliSieZgadza => IloscSymboliSieZgadza
        );


	
	SymbolsArray <= ('0','1','1',others => '1'), ('1','1','1',others => '0') after 200 ns;

END;
