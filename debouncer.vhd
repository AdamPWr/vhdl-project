----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:21:40 04/19/2020 
-- Design Name: 
-- Module Name:    debouncer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debouncer is
    Port ( Button : in  STD_LOGIC;
			  CLK		:	in STD_LOGIC;
           Is_Pressed : out  STD_LOGIC);
end debouncer;


architecture Behavioral of debouncer is

constant LICZNIK_LIMIT : integer := 5; -- 5* 20ns
signal licznik : STD_LOGIC_VECTOR(24 downto 0) := (others => '0'); 
signal jestNadalPrzytrzymany : STD_LOGIC := '0';

begin

liczenie_taktow_zegara : process(CLK)
   begin
	if Button = '0' then															-- zakladam ze jest pullup resistor
      if rising_edge(Clk) then
         if (unsigned(licznik) =
			LICZNIK_LIMIT) then
             licznik <= (others => '0');									-- zerowanie licznika
				 jestNadalPrzytrzymany <= not jestNadalPrzytrzymany;
         else
            licznik <= std_logic_vector(unsigned(licznik) + 1);  	-- inkrementacja licznika   
         end if;
      end if;
	end if;
end process liczenie_taktow_zegara ;

Is_Pressed <= jestNadalPrzytrzymany and not Button;
end Behavioral;

