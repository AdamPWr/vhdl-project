--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:45:20 06/08/2020
-- Design Name:   
-- Module Name:   D:/buttonDebiuncer/buttonDebouncer/spr_wyniku_test.vhd
-- Project Name:  buttonDebouncer
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: sprWyniku
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY spr_wyniku_test IS
END spr_wyniku_test;
 
ARCHITECTURE behavior OF spr_wyniku_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT sprWyniku
    PORT(
         BUTTON : IN  std_logic;
         STAN_GRY : IN  std_logic;
         LED : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal BUTTON : std_logic := '0';
   signal STAN_GRY : std_logic := '0';

 	--Outputs
   signal LED : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: sprWyniku PORT MAP (
          BUTTON => BUTTON,
          STAN_GRY => STAN_GRY,
          LED => LED
        );
		  
		  
	BUTTON 	<= '0','0' after 100 ns,'1' after 200 ns,'1' after 300 ns;
	STAN_GRY <= '0','1' after 100 ns,'1' after 200 ns,'0' after 300 ns;

	
END;
