----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:32:10 04/20/2020 
-- Design Name: 
-- Module Name:    sprWyniku - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sprWyniku is
    Port ( BUTTON : in  STD_LOGIC;
           STAN_GRY : in  STD_LOGIC;
           LED : out  STD_LOGIC);
end sprWyniku;

architecture Behavioral of sprWyniku is
	
	signal INPUTS : STD_LOGIC_VECTOR(1 downto 0) := "00";
	
begin
		-- zakladam ze zwarcie z masa zapala diode 

	with INPUTS select LED <=
	'0' when "11",
	'1' when others;
	
end Behavioral;

